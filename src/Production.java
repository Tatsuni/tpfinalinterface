
public class Production extends Thread {
	
	private Controlleur controlleur;
	
	public Production(Controlleur a_controlleur) {
		super();
		this.controlleur = a_controlleur;
	}

	public void run() {
		try {
			Recette recetteCourante = Recette.getRecetteByID(1);
			boolean stop = false;
			boolean startProd = false;
			while (!stop) {
				if (ProduitsFinis.getNombreTablesTotal(recetteCourante.getRecetteID()) > 320) {
					startProd = false;
					while (!startProd) {
						if (ProduitsFinis.getNombreTablesTotal(recetteCourante.getRecetteID()) < 320) {
							startProd = true;
						}
					}
				} else {
					int QtyPattes = Materiaux.getTotalPattes();
					int QtyDessus = Materiaux.getTotalDessus();
					int QtyPeinture = Materiaux.getTotalPeinture();
					if (QtyPattes > recetteCourante.getQtyPattes()) {
						if (QtyDessus > recetteCourante.getQtyDessus()) {
							if (QtyPeinture > recetteCourante.getQtyPeinture()) {
								for (int i = 0; i < 8; i++) {
									for (int j = 0; j < 4; i++) {
										this.controlleur.produire(recetteCourante.getRecetteID());
										Production.sleep(250);																	
									}
								}
								Production.sleep(1000 * 16);
							} else {
								Commandes.commanderPeinture(recetteCourante.getRecetteID());
							}
						} else {
							Commandes.commanderDessus(recetteCourante.getRecetteID());
						}
					} else {
						Commandes.commanderPattes(recetteCourante.getRecetteID());
					}					
				}
			}
		} catch (Exception e) {}
	}	
}
