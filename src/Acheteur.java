
public class Acheteur extends Thread {
	private Controlleur controlleur;
	
	public Acheteur(Controlleur a_controlleur) {
		super();
		this.controlleur = a_controlleur;
	}
	
	public void run() {
		boolean stopAchat = false;
		Client client = new Client(1, "Xavier", "Blanchette-Noel");
		try {
			while (!stopAchat) {
				Thread.sleep(1000 * 8);
				this.controlleur.acheter(1, client);
				Thread.sleep(1000 * 16);
			}
		} catch (Exception e) {}
	}
}
