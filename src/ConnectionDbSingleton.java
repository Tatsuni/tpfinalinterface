import java.sql.Connection;
import java.sql.DriverManager;


public class ConnectionDbSingleton {
	private static Connection instance;
	
	public ConnectionDbSingleton() {
		
	}
	
    public static Connection getInstance() throws Exception
    { 
        if (instance == null) {
            Connection connection = null;
            //Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:sqlite:../data/BDTPFinal.db";
            connection = DriverManager.getConnection(url);
            
            instance = connection;
        }
  
        return instance;
    }
}
