import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Recette extends EntiteBD {
	
	private String Name;
	private int QtyPattes;
	private int QtyDessus;
	private int QtyPeinture;
	
	public Recette()
	{}
	
	public Recette(int a_ID, String a_name, int a_QtyPattes, int a_QtyDessus, int a_QtyPeinture)
	{
		super(a_ID);
		this.Name = a_name;
		this.QtyPattes = a_QtyPattes;
		this.QtyDessus = a_QtyDessus;
		this.QtyPeinture = a_QtyPeinture;
	}
	
	public int getRecetteID() {
		return this.ID;
	}
	
	public int getQtyPattes() {
		return this.QtyPattes;
	}
	
	public int getQtyDessus() {
		return this.QtyDessus;
	}
	
	public int getQtyPeinture() {
		return this.QtyPeinture;
	}
	
	public static Recette getRecetteByID(int a_ID) throws Exception {
		String sql = "SELECT ID, Name, QtyPattes, QtyDessus, QtyPeinture FROM Recette WHERE ID=" + Integer.toString(a_ID);
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int ID = 0;
		String name = "";
		int QtyPattes = 0;
		int QtyDessus = 0;
		int QtyPeinture = 0;
		
		while (rs.next()) {
			ID = rs.getInt("ID");
			name = rs.getString("Name");
			QtyPattes = rs.getInt("QtyPattes");
			QtyDessus = rs.getInt("QtyDessus");
			QtyPeinture = rs.getInt("QtyPeinture");
		}
		
		Recette r = new Recette(ID, name, QtyPattes, QtyDessus, QtyPeinture);
		
		return r;
	}
	
	public void save() throws Exception {
		if (this.Tracked) {
			update();
		} else {
			insert();
			this.Tracked = true;
		}
	}
	
	private void update() throws Exception {
		String sql = "UPDATE Recette SET Name=?, QtyPattes=?, QtyDessus=?, QtyPeinture=? WHERE ID=?";
		Connection connection = ConnectionDbSingleton.getInstance();
		
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, this.Name);
		ps.setInt(2, this.QtyPattes);
		ps.setInt(3, this.QtyDessus);
		ps.setInt(4, this.QtyPeinture);
		ps.setInt(5, this.ID);
		
		int affectedRows = ps.executeUpdate();
		
		if (affectedRows == 0) {
			throw new Exception();
		}
	}
	
	private void insert() throws Exception {
		String sql = "INSERT INTO Recette(name, QtyPattes, QtyDessus, QtyPeinture) VALUES(?, ?, ?, ?)";
		Connection connection = ConnectionDbSingleton.getInstance();
		
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, this.Name);
		ps.setInt(2, this.QtyPattes);
		ps.setInt(3, this.QtyDessus);
		ps.setInt(4, this.QtyPeinture);
		
		int affectedRows = ps.executeUpdate();
		
		if (affectedRows == 0) {
			throw new Exception();
		}
		
		ResultSet rs = ps.getGeneratedKeys();
		
		if (rs.next()) {
			this.ID = rs.getInt("ID");
		} else {
			throw new Exception();
		}
	}
}
