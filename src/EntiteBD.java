abstract public class EntiteBD {
	protected int ID;
	
	protected boolean Tracked;
	
	public EntiteBD()
	{}
	
	public EntiteBD(int a_ID) {
		this();
		this.ID = a_ID;
	}
	
	public void Track() {
		this.Tracked = true;
	}
	
	public void unTrack() {
		this.Tracked = false;
	}
}
